#!/usr/bin/env python
# Import the os module, for the os.walk function
import os
import sys
import subprocess
 
# Set the directory you want to start from
rootDir = '.'
if len(sys.argv) > 1:
	arg = str(sys.argv[1]) 
	if len(arg) > 0:
		rootDir = arg
removeOrig = False
for arg in sys.argv:
	if arg == "remove":
		removeOrig = True
		break
print("\n")
for dirName, subdirList, fileList in os.walk(rootDir):
	for fname in fileList:
		fullpath = os.path.join(dirName, fname)
		if fullpath.endswith(".png"):
			print("processing "+fullpath+" ... ")
			call = []
			call.append("cwebp")
			call.append("-q")
			call.append("100")
			call.append("-lossless")
			call.append(fullpath)
			call.append("-o")
			call.append(fullpath.replace(".png", ".webp"))
			
			subprocess.call(call)	
			print(" Done!\n")
			if removeOrig:
				os.remove(fullpath)
